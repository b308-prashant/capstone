import matplotlib.pyplot as plt

students = {}
grades = {}

def add_student():
    name = input("New student name: ")
    grade = int(input("New student grade: "))
    students[name] = grade
    grades.setdefault(grade, []).append(name)
    print("Student and grade added successfully.")

def print_students():
    if students:
        print("All Students:")
        for student in students:
            print(student)
    else:
        print("No students added yet.")

def print_grades():
    if grades:
        print("All Grades:")
        for grade in sorted(grades.keys()):
            print(f"Grade {grade}: {grades[grade]}")
    else:
        print("No grades added yet.")

def lookup_grade():
    name = input("Enter student name to look up grade: ")
    if name in students:
        print(f"Student {name} has grade {students[name]}")
    else:
        print(f"Student does not exist")

def highest_grade():
    if students:
        highest_student = max(students, key=students.get)
        print(f"The student with the highest grade is {highest_student}. His or her grade is {students[highest_student]}.")
    else:
        print("No students added yet.")

def lowest_grade():
    if students:
        lowest_student = min(students, key=students.get)
        print(f"The student with the lowest grade is {lowest_student}. His or her grade is {students[lowest_student]}.")
    else:
        print("No students added yet.")

def generate_graph():
    grade_ranges = ['71-80', '81-90', '91-95', '96-100']
    counts = [0] * len(grade_ranges)
    total_students = len(students)

    for grade in students.values():
        if 71 <= grade <= 80:
            counts[0] += 1
        elif 81 <= grade <= 90:
            counts[1] += 1
        elif 91 <= grade <= 95:
            counts[2] += 1
        elif 96 <= grade <= 100:
            counts[3] += 1

    percentages = [count / total_students * 100 for count in counts]

    plt.pie(percentages, labels=grade_ranges, autopct='%1.1f%%')
    plt.title('Percentage Distribution of Marks')
    plt.show()

def quit_program():
    print("Quitting the program...")
    exit()

while True:
    print("""
    Choose an option:
    1. Add a new student & grade
    2. Print all students
    3. Print all grades
    4. Look up a student's grade
    5. Find the student with the highest grade
    6. Find the student with the lowest grade
    7. Generate Graph 
    8. Quit
    """)

    option = input("Choose an option: ")

    if option == '1':
        add_student()
    elif option == '2':
        print_students()
    elif option == '3':
        print_grades()
    elif option == '4':
        lookup_grade()
    elif option == '5':
        highest_grade()
    elif option == '6':
        lowest_grade()
    elif option == '7':
        generate_graph()
    elif option == '8':
        quit_program()
    else:
        print
